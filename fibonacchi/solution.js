// We have 2 ways how to find requested number
// First way: simple recursion
// At first view it is a great idea but simple recursion with "if else" recursion will take a lot of time
// if we try to find 10-15th number or more
// Second way: according to Binet's formula we can do this

function fibonacci(n) {
    let rof = Math.sqrt(5);
    let a = (1 + rof) / 2;
    let b = (1 - rof) / 2;
    return Math.round((Math.pow(a, n) - Math.pow(b, n)) / rof); //add Math.round because result might be incorrect

}
//e.g.
//fibonacci(5);

